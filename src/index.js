const express = require('express');
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser');
const port = 3000;

app.use(express.json());
app.use(cors({
    origin: '*',
    credentials: true
}));

// app.use(bodyParser.json());

app.post('/multiplicar', (peticion, respuesta) => {
    let number1 = peticion.body.number1;
    let number2 = peticion.body.number2;

    if ( isNaN(number1) || isNaN(number2)) {
        respuesta.status(400).json({message: 'Números inválidos'});
    } else {
        let multiplicar = parseFloat(number1) * parseFloat(number2);
        respuesta.json({resultado: multiplicar});
    }
});

app.listen(port, () => {
    console.log('Servidor activo en http://localhost:' + port);
});


// const express = require('express'); //llamamos a Express
// const bodyParser = require('body-parser');
// const cors = require('cors');
// const app = express();               

// const port = process.env.PORT || 8080;  // establecemos nuestro puerto

// // app.use(express.urlencoded({ extended: false }));
// app.use(express.json());

// // app.use(bodyParser.urlencoded({ extended: false }));
// // app.use(bodyParser.json());


// app.use(cors({
//     origin: '*',
//     credentials: true
// }));

// app.use(bodyParser.json());


// app.post('/potenciacion', cors(), function (req, res) {
//     if(!req.body.Num1 || !req.body.Num2) {
//         respuesta = {
//             error: true
//         };
//     } else {
//         let Num1 = req.body.Num1;
//         let Num2 = req.body.Num2;
//         let res = Math.pow(parseFloat(Num1), parseFloat(Num2));
        
//         respuesta = {
//             resultado: res
//         };
//     }
    
//     res.send(respuesta);
// });

// app.post('/radicacion', cors(), function (req, res) {
//     if(!req.body.Num1 || !req.body.Num2) {
//         respuesta = {
//             error: true
//         };
//     } else {
//         let Num1 = req.body.Num1;
//         let Num2 = req.body.Num2;
//         let res = Math.pow(parseFloat(Num1), 1/parseFloat(Num2));
        
//         respuesta = {
//             resultado: res
//         };
//     }
    
//     res.send(respuesta);
// });

// // iniciamos nuestro servidor
// app.listen(port)
// console.log('API escuchando en el puerto ' + port)






// const express = require("express");
// const bodyParser = require('body-parser');
// const cors = require('cors');
// const app = express();

// app.use(express.urlencoded({ extended: false }));
// app.use(express.json());

// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());

// app.use(cors({
//     origin: '*',
//     methods: ['GET','POST']
// }));

// app.use(function (req, res, next) {
//     res.setHeader('Access-Control-Allow-Origin', '*');
//     res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
//     res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
//     res.setHeader('Access-Control-Allow-Credentials', true);

//     next();
// });

// app.get('/', cors(), function(req, res) {
//     respuesta = {
//         error: true,
//         codigo: 200,
//         mensaje: 'Punto de inicio'
//     };
    
//     res.send(respuesta);
// });

// app.post('/suma', cors(), function (req, res) {
//     if(!req.body.Num1 || !req.body.Num2) {
//         respuesta = {
//             error: true,
//             codigo: 502,
//             mensaje: 'Num1 y Num2 son requeridos'
//         };
//     } else {
//         let Num1 = req.body.Num1;
//         let Num2 = req.body.Num2;
//         let res = Num1 + Num2;
        
//         respuesta = {
//             error: false,
//             codigo: 200,
//             resultado: res
//         };
//     }
    
//     res.send(respuesta);
// });

// app.post('/resta', cors(), function (req, res) {
//     if(!req.body.Num1 || !req.body.Num2) {
//         respuesta = {
//             error: true,
//             codigo: 502,
//             mensaje: 'Num1 y Num2 son requeridos'
//         };
//     } else {
//         let Num1 = req.body.Num1;
//         let Num2 = req.body.Num2;
//         let res = Num1 - Num2;
        
//         respuesta = {
//             error: false,
//             codigo: 200,
//             resultado: res
//         };
//     }
    
//     res.send(respuesta);
// });

// app.post('/multiplicacion', cors(), function (req, res) {
//     if(!req.body.Num1 || !req.body.Num2) {
//         respuesta = {
//             error: true,
//             codigo: 502,
//             mensaje: 'Num1 y Num2 son requeridos'
//         };
//     } else {
//         let Num1 = req.body.Num1;
//         let Num2 = req.body.Num2;
//         let res = Num1*Num2;
        
//         respuesta = {
//             error: false,
//             codigo: 200,
//             resultado: res
//         };
//     }
    
//     res.send(respuesta);
// });

// app.post('/division', cors(), function (req, res) {
//     if(!req.body.Num1 || !req.body.Num2) {
//         respuesta = {
//             error: true,
//             codigo: 502,
//             mensaje: 'Num1 y Num2 son requeridos'
//         };
//     } else {
//         let Num1 = req.body.Num1;
//         let Num2 = req.body.Num2;
//         let res = Num1/Num2;
        
//         respuesta = {
//             error: false,
//             codigo: 200,
//             resultado: res
//         };
//     }
//     res.send(respuesta);
// });

// app.listen(3000, () => {
// 	console.log("El servidor está inicializado en el puerto 3000");
// });